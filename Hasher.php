<?php

namespace Acgranter\Hasher;

use Symfony\Component\PasswordHasher\Exception\InvalidPasswordException;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Hasher implements PasswordHasherInterface
{
  const DEFAULT_ALGORITHM = "shake256";
  const DEFAULT_SALT = "";

  private $cmd;
  private $algorithm;
  private $salt;

  public function __construct(array $config = [])
  {
    $this->cmd = $config['cmd'] ?? '/usr/bin/acgranter-hash';
    $this->algorithm = $config['algorithm'] ?? self::DEFAULT_ALGORITHM;
    $this->salt = $config['salt'] ?? self::DEFAULT_SALT;
  }
  /**
   * Hashes a plain password.
   *
   * @throws InvalidPasswordException When the plain password is invalid, e.g. excessively long
   * @throws ProcessFailedException When execution of acgranter-hash command failed
   */
  public function hash(string $plainPassword): string{
    $process = new Process([$this->cmd,
      '--hash', $this->algorithm,
      '--salt', $this->salt
    ], null, null, $plainPassword);
    $process->run();
    $output = $process->getOutput();
    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
    }
    return trim($output);
  }

  /**
   * Verifies a plain password against a hash.
   */
  public function verify(string $hashedPassword, string $plainPassword): bool{
    return $this->hash($plainPassword) === $hashedPassword;
  }

  /**
   * Checks if a password hash would benefit from rehashing.
   */
  public function needsRehash(string $hashedPassword): bool{
    return false;
  }
}
