<?php

namespace Acgranter\Hasher;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

/**
 * PasswordHasherFactoryInterface to support different password hashers for different user accounts.
 *
 * @author Robin Chalas <robin.chalas@gmail.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
class HasherFactory implements PasswordHasherFactoryInterface
{
  private $hashers = [];
  private $cmd = '';

  /**
   * @param array $x
   */
  public function __construct(array $hashers = [], $cmd = '/usr/bin/acgranter-hash')
  {
    $this->hashers = $hashers;
    $this->cmd = $cmd;
  }

  /**
   * @param $classOrObject
   * @param string $name
   * @throws \RuntimeException
   * @return PasswordHasherInterface
   */
  public function getPasswordHasher($classOrObject, $name = ''): PasswordHasherInterface{
    return $this->getHasher($classOrObject, $name);
  }

  /**
   * @param $classOrObject
   * @param string $name
   * @throws \RuntimeException
   * @return PasswordHasherInterface
   */
  public function getHasher($classOrObject, $name = ''): PasswordHasherInterface{
    $class = \is_object($classOrObject) ? get_debug_type($classOrObject) : (string) $classOrObject;

    if ($name !== ''){
      if (isset($this->hashers[$name])){
        return $this->createHasher($this->hashers[$name]);
      }
    }else{
      foreach ($this->hashers as $hasher){
        if (empty($hasher['entities']) || in_array($class, $hasher['entities'])){
          return $this->createHasher($hasher);
        }
      }
    }
    throw new \RuntimeException(sprintf('No hasher has been configured for class "%s".', \is_object($class) ? get_debug_type($class) : $class));
  }

  private function createHasher($hasherConfig): PasswordHasherInterface{
    if (isset($hasherConfig['id'])){
      /**
       * @var $hasher PasswordHasherInterface
       */
      $hasher = $hasherConfig['id'];
      if ( !($hasher instanceof PasswordHasherInterface) ){
        throw new \RuntimeException(sprintf(
          'Service "%s" does not implement %s.',
          \is_object($hasher) ? get_debug_type($hasher) : $hasher, PasswordHasherInterface::class
        ));
      }
      return $hasherConfig['id'];
    }else{
      $config = [
        'algorithm' => $hasherConfig['algorithm'] ?? Hasher::DEFAULT_ALGORITHM,
        'salt' => $hasherConfig['salt'] ?? Hasher::DEFAULT_SALT,
        'cmd' => $this->cmd
      ];
      return new Hasher($config);
    }
  }

}
